/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/


package org.fly.swing.utils;

import java.awt.Color;

import javax.swing.JColorChooser;

import org.fly.core.utils.ColorUtil;

public class FLYColorChooser {
	
	public static Color getColor(Color color){
		
		return JColorChooser.showDialog(null, "", color);
	}

	public static Integer chooseColor(Integer color){
		Color c = null;
		if(color != null)
			c = new Color(color.intValue());
		 Color res = JColorChooser.showDialog(null, "", c);
		 if(res == null)
			 return null;
		 int r = res.getRed();
		 int g = res.getGreen();
		 int b = res.getBlue();
		 return ColorUtil.getIntegerFromRGB(r, g, b);
	}
	
	public static Integer chooseColor(Color color){
		return chooseColor(ColorUtil.getIntegerFromColor(color));
	}

	

	public static void main(String[] args) {
		
		Integer i = chooseColor(0);
		if(i != null){
			System.out.println(i.intValue());
			Integer j = chooseColor(i);
			if(j != null)
				chooseColor(j);
		}
		
		
	}

	
}
