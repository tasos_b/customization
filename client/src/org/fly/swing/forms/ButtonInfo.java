package org.fly.swing.forms;


import java.io.Serializable;
import java.math.BigDecimal;

import javax.swing.ImageIcon;

public class ButtonInfo implements Serializable{
	
	private BigDecimal m_foreground;
	private BigDecimal m_background;
	private String m_title;
	private BigDecimal AD_Image_ID;
	private ImageIcon image;

	public BigDecimal getAD_Image_ID() {
		return AD_Image_ID;
	}
	public void setAD_Image_ID(BigDecimal aD_Image_ID) {
		AD_Image_ID = aD_Image_ID;
	}
	
	public ImageIcon getImage() {
		return image;
	}
	
	public void setImage(ImageIcon image) {
		this.image = image;
	}
	
	public BigDecimal getM_foreground() {
		return m_foreground;
	}
	
	public void setM_foreground(BigDecimal m_foreground) {
		this.m_foreground = m_foreground;
	}
	
	public BigDecimal getM_background() {
		return m_background;
	}
	
	public void setM_background(BigDecimal m_background) {
		this.m_background = m_background;
	}
	
	public String getM_title() {
		return m_title;
	}
	
	public void setM_title(String m_title) {
		this.m_title = m_title;
	}
	

}
