package org.fly.swing.forms;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;

import org.compiere.apps.AppsAction;
import org.compiere.apps.ConfirmPanel;
import org.compiere.grid.ed.ImagePreviewPanel;
import org.compiere.model.GridTab;
import org.compiere.swing.CButton;
import org.compiere.swing.CDialog;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CTextField;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.ColorUtil;
import org.fly.swing.utils.FLYColorChooser;

public class ButtonInfoDialog extends CDialog {


	private static final long serialVersionUID = -6526031133601085088L;

	private ConfirmPanel m_confirmPanel = new ConfirmPanel(true);

	private BorderLayout m_layout = new BorderLayout();

	private CPanel m_panel = new CPanel();
	private CPanel m_controlPanel = new CPanel();

	private byte[] m_data = null;

	private ButtonInfo m_info = null;

	private GridTab m_tab = null;

	private CLabel l_title = new CLabel(Msg.translate(Env.getCtx(), "Title"));
	private CLabel l_foreground = new CLabel(Msg.translate(Env.getCtx(),
			"Foreground color"));
	private CLabel l_background = new CLabel(Msg.translate(Env.getCtx(),
			"Background color"));
	private CLabel l_image = new CLabel(Msg.translate(Env.getCtx(), "Image"));
	private CTextField f_title = new CTextField();
	private CButton b_foreground = new CButton();
	private CButton b_background = new CButton();
	private ImagePreviewPanel p_image = null;

	public Dimension labelDimension = new Dimension(200, 30);
	public Dimension ComponentDimension = new Dimension(200, 30);

	public ButtonInfoDialog(String title, byte[] data, GridTab tab)
			throws HeadlessException {
		this(null, title, true, data, tab);
	}

	public ButtonInfoDialog(Frame owner, String title, boolean modal,
			byte[] data, GridTab tab) throws HeadlessException {

		super(owner, title, false);
		m_data = data;
		m_tab = tab;
		if (m_data == null) {
			m_info = new ButtonInfo();
		} else {
			m_info = ByteArrayUtil.byteArrayToButtonInfo(m_data);

		}

		initComponents();
	}

	private void initComponents() {

		getContentPane().add(m_panel);
		m_panel.setLayout(m_layout);
		m_panel.add(m_confirmPanel, BorderLayout.SOUTH);
		m_panel.add(m_controlPanel, BorderLayout.CENTER);
		m_confirmPanel.addActionListener(this);
		JButton addImage = createButtonAction(
				Msg.translate(Env.getCtx(), "Select Image"),
				KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, 0));
		JButton deleteImage = createButtonAction(
				Msg.translate(Env.getCtx(), "Delete Image"),
				KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, 0));
		m_confirmPanel.addButton(addImage);
		m_confirmPanel.addButton(deleteImage);
		ImageIcon ico = null;
		if (m_info != null && m_info.getImage() != null)
			ico = m_info.getImage();
		p_image = new ImagePreviewPanel(ico);
		f_title.setText(m_info.getM_title());

		addPairControl(l_title, labelDimension, f_title, ComponentDimension);
		addPairControl(l_foreground, labelDimension, b_foreground,
				ComponentDimension);
		addPairControl(l_background, labelDimension, b_background,
				ComponentDimension);
		addPairControl(l_image, labelDimension, p_image,
				new Dimension(250, 250));
		if (m_info.getM_foreground() != null)
			b_foreground.setBackground(ColorUtil.getColorFromInteger(m_info
					.getM_foreground().intValue()));
		if (m_info.getM_background() != null)
			b_background.setBackground(ColorUtil.getColorFromInteger(m_info
					.getM_background().intValue()));
		b_foreground.addActionListener(this);
		b_background.addActionListener(this);

	}

	private void addPairControl(CLabel label, Dimension labelDimension,
			Component component, Dimension componentDimension) {
		label.setPreferredSize(labelDimension);
		component.setPreferredSize(componentDimension);
		CPanel panel = new CPanel();
		panel.setLayout(new FlowLayout());
		panel.add(label);
		panel.add(component);
		m_controlPanel.add(panel);
	}

	protected CButton createButtonAction(String action, KeyStroke accelerator) {
		AppsAction act = new AppsAction(action, accelerator, false);
		act.setDelegate(this);
		CButton button = (CButton) act.getButton();
		button.setFocusable(false);
		return button;
	} // getButtonAction

	public void actionPerformed(ActionEvent e) {
		String actImage = Msg.translate(Env.getCtx(), "Select Image");
		String delImage = Msg.translate(Env.getCtx(), "Delete Image");
		if (e.getActionCommand().equals(ConfirmPanel.A_CANCEL))
			dispose();
		else if (e.getActionCommand().equals(ConfirmPanel.A_OK)) {
			m_info.setM_title(f_title.getText());
			m_info.setImage(p_image.getIcon());
			byte[] data = ByteArrayUtil.buttonInfoToByteArray(m_info);
			m_tab.setValue("buttoninfo", data);
			dispose();
		} else if (actImage.equals(e.getActionCommand())) {
			ImageIcon icon = selectIcon();
			if (icon != null)
				p_image.setImage(icon);

		}else if (delImage.equals(e.getActionCommand())) {
				p_image.setImage(null);
				repaint();
		} 
		else if (e.getSource() == b_foreground) {
			Integer iColor = null;
			if (m_info.getM_foreground() != null)
				iColor = m_info.getM_foreground().intValue();
			iColor = FLYColorChooser.chooseColor(iColor);
			if (iColor != null) {
				m_info.setM_foreground(new BigDecimal(iColor));
				b_foreground.setBackground(ColorUtil
						.getColorFromInteger(iColor));
			} else {
				m_info.setM_foreground(null);
				b_foreground.setBackground(new JButton().getBackground());

			}
		} else if (e.getSource() == b_background) {
			Integer iColor = null;
			if (m_info.getM_background() != null)
				iColor = m_info.getM_background().intValue();
			iColor = FLYColorChooser.chooseColor(iColor);
			if (iColor != null) {
				m_info.setM_background(new BigDecimal(iColor));
				b_background.setBackground(ColorUtil
						.getColorFromInteger(iColor));
			}
			else{
				m_info.setM_background(null);
				b_background.setBackground(new JButton().getBackground());
			}
		}

	}

	private ImageIcon selectIcon() {
		JFileChooser jfc = new JFileChooser();
		jfc.setMultiSelectionEnabled(false);
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		ImagePreviewPanel preview = new ImagePreviewPanel();
		jfc.setAccessory((JComponent) preview);
		jfc.addPropertyChangeListener(
				JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, preview);
		jfc.showOpenDialog(preview);

		// Get File Name
		File imageFile = jfc.getSelectedFile();
		if (imageFile == null || imageFile.isDirectory() || !imageFile.exists())
			return null;

		String fileName = imageFile.getAbsolutePath();
		byte[] data = null;

		try {
			FileInputStream fis = new FileInputStream(imageFile);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024 * 8]; // 8kB
			int length = -1;
			while ((length = fis.read(buffer)) != -1)
				os.write(buffer, 0, length);
			fis.close();
			data = os.toByteArray();
			os.close();
			ImageIcon image = new ImageIcon(data, fileName);
			return image;
		} catch (Exception e) {

			return null;
		}

	}

}
