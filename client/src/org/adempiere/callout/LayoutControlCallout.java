/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.adempiere.callout;

import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.AdempiereSystemError;
import org.fly.core.utils.FLYProcessUtil;

//TODO To Cache them
public class LayoutControlCallout extends CalloutEngine{
	public String changeControl(Properties ctx, int windowNo, GridTab mTab, 
			GridField mField, Object value) throws AdempiereSystemError { 
		if(value != null){
			int val = (int)value;
			if(val == 102) 
				mTab.setValue("AD_Process_ID", FLYProcessUtil.getProcessID("Product_Category_Btn_Group") );
			else if(((int)value) == 103)
				mTab.setValue("AD_Process_ID",  FLYProcessUtil.getProcessID("Products_Btn_Group"));
			else if(((int)value) == 107)
				mTab.setValue("AD_Process_ID", FLYProcessUtil.getProcessID("Product_Classification_Btn_Group"));
			else if(((int)value) == 108)
				mTab.setValue("AD_Process_ID", FLYProcessUtil.getProcessID("Product_Class_Btn_Group"));
			else if(((int)value) == 109)
				mTab.setValue("AD_Process_ID", FLYProcessUtil.getProcessID("Product_Group_Btn_Group"));
			else if(((int)value) == 100)
				mTab.setValue("AD_Process_ID", FLYProcessUtil.getProcessID("Free_Buttons"));
			else if(((int)value) == 101)
				mTab.setValue("AD_Process_ID", FLYProcessUtil.getProcessID("Receipt_Lines"));
			else if(((int)value) == 110)
				mTab.setValue("AD_Process_ID", FLYProcessUtil.getProcessID("Receipt_Infos"));
			
			return "";
		}
		return "";
		
	}
}
