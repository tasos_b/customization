package org.adempiere.callout;

import java.awt.Dimension;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.fly.swing.forms.ButtonInfoDialog;


public class ButtonInfoCallout extends CalloutEngine{
	
	public String open(Properties ctx, int windowNo, GridTab mTab, 
			GridField mField, Object value) throws AdempiereSystemError { 
		byte[] data = null;
		
		if(mTab.getValue("buttoninfo") != null)
			data = (byte[])mTab.getValue("buttoninfo");
		ButtonInfoDialog dlg = new ButtonInfoDialog( Msg.translate(Env.getCtx(), "Button Info Dialog"), data, mTab);
		dlg.setSize(new Dimension(500, 600));
		dlg.setLocationRelativeTo(null);
		dlg.setVisible(true);
		return null;
	}

}
