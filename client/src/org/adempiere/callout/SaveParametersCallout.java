/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/


package org.adempiere.callout;

import org.compiere.apps.FLYProcessDialog;
import org.compiere.model.CalloutEngine;

import java.awt.Dimension;
import java.util.Properties; 

import org.compiere.model.GridField; 
import org.compiere.model.GridTab; 
import org.compiere.process.ProcessInfo;
import org.compiere.util.AdempiereSystemError; 
import org.fly.core.utils.ByteArrayUtil;



public class SaveParametersCallout extends CalloutEngine{

	public String open(Properties ctx, int windowNo, GridTab mTab, 
			GridField mField, Object value) throws AdempiereSystemError { 
		ProcessInfo pi = null;
		Integer AD_Process_ID = null;
		if(mTab.getValue("AD_Process_ID") != null)
			AD_Process_ID = (int)mTab.getValue("AD_Process_ID");
		//prevent circular calls
		if(value != null){
			mTab.setValue("runbutton", null);
			return null;
		}
		//extract data if any, or create a new processinfo 
		byte[] data = ((byte[])mTab.getValue("data"));
		if(data != null){
			pi = ByteArrayUtil.byteArrayToProcessInfo( data);
		}
		else{
			pi = new ProcessInfo("Action", AD_Process_ID);
			if(mTab.getValue("AD_Client_ID") != null)
				pi.setAD_Client_ID((int)mTab.getValue("AD_Client_ID"));
		}
		pi.setAD_Process_ID(AD_Process_ID);
		//open Param dialog
		FLYProcessDialog dlg = new FLYProcessDialog(null, AD_Process_ID, true, pi, mTab);
		dlg.setSize(new Dimension(800, 600));
		dlg.setPi(pi);
		dlg.init();
		dlg.setLocationRelativeTo(null);
		dlg.setVisible(true);
		
		return null;
	}

	
	public String onChangeProcess(Properties ctx, int windowNo, GridTab mTab, 
			GridField mField, Object value) throws AdempiereSystemError { 
		mTab.setValue("data", null);
		return null;
	}

}
