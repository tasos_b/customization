/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/


package org.fly.core.utils;

import java.awt.Color;

public class StringUtil {

	public StringUtil() {
		// TODO Auto-generated constructor stub
	}
	
	public static String toHtml(String plain){
		return "<html>" + plain + "</html>";
	}

	public static String stringToHtml(String plain, Integer color){
		String colorStr = ColorUtil.getHtmlColorFromInteger(color);
		return stringToHtml(plain, colorStr);
	}
	
	public static String stringToHtml(String plain, Color color){
		String colorStr = ColorUtil.getHtmlColorFromColor(color);
		return stringToHtml(plain, colorStr);
	}

	
	public static String stringToHtml(String plain, String color){
		String res = 	"<html>" +
						"<body>" +
						"<p style=\"color:" +
						color +
						"\">" +
						plain + 
						"</p>" +
						"</body></html>";
		return res;
	}

	public static String stringToHtml(String plain, Integer color, int sizePx){
		String colorStr = ColorUtil.getHtmlColorFromInteger(color);
		return stringToHtml(plain, colorStr, sizePx);
	}

	
	public static String stringToHtml(String plain, Color color, int sizePx){
		String colorStr = ColorUtil.getHtmlColorFromColor(color);
		return stringToHtml(plain, colorStr, sizePx);
	}

	
	public static String stringToHtml(String plain, String color, int sizePx){
		String res = 	"<html>" +
						"<body>" +
						"<p style=\"color:" +
						color + ";font-size:" + Integer.toString(sizePx) + "px"+
						"\">" +
						plain + 
						"</p>" +
						"</body></html>";
		return res;
	}

	public static String stringToHtml(String plain, Integer color, Integer bgColor, int sizePx){
		String colorStr = ColorUtil.getHtmlColorFromInteger(color);
		String colorBgStr = ColorUtil.getHtmlColorFromInteger(bgColor);
		return stringToHtml(plain, colorStr, colorBgStr, sizePx);
	}

	
	public static String stringToHtml(String plain, Color color, Color bgColor, int sizePx){
		String colorStr = ColorUtil.getHtmlColorFromColor(color);
		String colorBgStr = ColorUtil.getHtmlColorFromColor(bgColor);
		return stringToHtml(plain, colorStr, colorBgStr, sizePx);
	}

	
	public static String stringToHtml(String plain, String color, String bgColor, int sizePx){
		String res = 	"<html>" +
						"<body>" +
						"<p style=\"color:" +
						color + 
						";font-size:" + Integer.toString(sizePx) + "px"+
						";background-color:" + bgColor +
						"\">" +
						plain + 
						"</p>" +
						"</body></html>";
		return res;
	}
	
	public static boolean isNullOrEmpty(String str){
		return (str == null) || (str.isEmpty());
	}
	
	public static String FormatRight(String text){
		int size = 30;
		if(text.length() < size){
			int diff = size - text.length();
			String tmp = "";
			for(int i = 0; i < diff; i++)
				tmp += " ";
			String result = tmp + text;
			return result;
		}
		return text;
	}

}
