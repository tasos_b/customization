package org.fly.core.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;

import org.compiere.util.Language;

public class FLYMath {
	public static BigDecimal stringToBigDecimal(String str){
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(Language.getLoginLanguage().getLocale());
        df.setParseBigDecimal(true);
        try {
			BigDecimal bd = (BigDecimal) df.parseObject(str);
			return bd;
		} catch (ParseException e) {
			
			e.printStackTrace();
			return null;
		}
	    
	}
	
	public static String bigDecimalToString(BigDecimal bd){
		return bd.stripTrailingZeros().toPlainString();
		
	}
	
	public static String getDecimalSeparator(){
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(Language.getLoginLanguage().getLocale());
		DecimalFormatSymbols symbols=df.getDecimalFormatSymbols();
		char sep=symbols.getDecimalSeparator();
		String result = "" + sep;
		return result;
	}
}
