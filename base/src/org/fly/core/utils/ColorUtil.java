/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.utils;

import java.awt.Color;

public class ColorUtil {
	public static int getIntegerFromColor(Color color){
		if(color == null)
			return 0;
		return new Color(color.getRed(), color.getGreen(), color.getBlue()).getRGB();
	}
	
	public static Color getColorFromInteger(Integer i){
		if(i == null)
			return null;
		return new Color(i.intValue());
	}
	
	public static int getIntegerFromRGB(int r, int g, int b){
		return new Color(r, g, b).getRGB();
	}
	
	public static String getHtmlColorFromColor(Color color){
		if (color == null)
			return null;
		int iRed = color.getRed();
		int iGreen = color.getGreen();
		int iBlue = color.getBlue();
		
		String red = fmtHex(Integer.toHexString(iRed));
		String green = fmtHex(Integer.toHexString(iGreen));
		String blue = fmtHex(Integer.toHexString(iBlue));
		return "#" + red + green + blue;
		
	}
	private static String fmtHex(String str){
		if(str.length() == 1)
			return "0" + str;
		return str;
	}
	public static String getHtmlColorFromInteger(Integer iColor){
		if(iColor == null)
			return null;
		Color color = getColorFromInteger(iColor);
		return getHtmlColorFromColor(color);
		
	}
	
	public static int convert(int n) {
		  return Integer.valueOf(String.valueOf(n), 16);
		}
}
