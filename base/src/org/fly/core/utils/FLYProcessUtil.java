/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.adempiere.exceptions.DBException;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;

/**
 *  ProcessInfo useful routines .

 * @author: tasos boulassikis tasosbull@gmail.com */
public class FLYProcessUtil {

	/**
	 * Get Parameter by a given name
	 * @param pi the ProcessInfo
	 * @param paramName the parameter name
	 * @return the ProcessInfoParameter found
	 * */
	public static ProcessInfoParameter getParameter(ProcessInfo pi,
			String paramName) {
		return getParameter(pi.getParameter(), paramName);
	}

	/**
	 * Get the value of a parameter parameter by a given parameter name
	 * @param pi the ProcessInfo
	 * @param paramName the parameter name
	 * @return the parameter value founded
	 * */
	public static Object getParameterValue(ProcessInfo pi,
			String paramName) {
		if(getParameter(pi, paramName) != null)
			return getParameter(pi.getParameter(), paramName).getParameter();
		return null;
	}

	
	
	/**
	 * Get Parameter by a given name
	 * @param params the ProcessInfoParameter[]
	 * @param paramName the parameter name
	 * @return the ProcessInfoParameter found
	 * */
	public static ProcessInfoParameter getParameter(
			ProcessInfoParameter[] params, String paramName) {
		if (params == null)
			return null;
		for (int i = 0; i < params.length; i++) {
			String p1 = params[i].getParameterName().trim().toUpperCase();
			String p2 = paramName.trim().toUpperCase();
			if (p1.equals(p2))
				return params[i];
		}
		return null;

	}

	
	/**
	 * Merge two ProcessInfo instances
	 * Add to the main instance all extras
	 * Override anyone that exists in main
	 * 
	 * @param main the result ProcessInfo
	 * @param extras the extra parameters
	 * @return the main ProcessInfo
	 * */
	public static ProcessInfo mergeProcessInfos(ProcessInfo main,
			ProcessInfo extras) {
		ProcessInfoParameter[] extraParams = extras.getParameter();
		ProcessInfoParameter[] mainParams = main.getParameter();
		if (extraParams != null) {
			for (int i = 0; i < extraParams.length; i++) {
				ProcessInfoParameter found = getParameter(mainParams,
						extraParams[i].getParameterName());
				if (found != null) {
					found.setParameter(extraParams[i].getParameter());
				} else {
					main.addParameter(extraParams[i].getParameterName(),
							extraParams[i].getParameter(),
							extraParams[i].getParameterName());
				}
			}
		}
		return main;
	}

	/**
	 * Set parameter in a ProcessInfo
	 * If exists just change the value
	 * @param pi ProcessInfo
	 * @param name text
	 * @param value Object 
	 * */
	public static void setParameter(ProcessInfo pi, String name, Object value) {
		ProcessInfoParameter[] params = pi.getParameter();
		ProcessInfoParameter found = FLYProcessUtil.getParameter(params, name);
		if (found != null) {
			found.setParameter(value);
		} else {
			pi.addParameter(name, value, name);
		}

	}
	/**
	 * Find the AD_Process_ID given a name
	 * @param value the process name
	 * @return the AD_Process_ID
	 * */
	public static int getProcessID(String value){
		String sql = "SELECT AD_Process_ID FROM AD_Process WHERE value=?";
		SQLUtil util = new SQLUtil(sql, null, value);
		try{
			ResultSet rs = util.getResultSet();
			if(rs.next())
				return rs.getInt("AD_Process_ID");
			return 0;
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

	}

}
