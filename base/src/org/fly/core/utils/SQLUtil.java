/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.adempiere.exceptions.DBException;
import org.compiere.util.DB;


public class SQLUtil {
	private String m_sql;
	private Object[] m_params;
	private String m_trx;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;
	
	public SQLUtil(String sql, String trxName, Object... params){
		m_sql = sql;
		m_params = params;
		m_trx = trxName;
	}

	public ResultSet getResultSet(){
		pstmt = DB.prepareStatement(m_sql, m_trx);
		try {
			DB.setParameters(pstmt, m_params);
			rs = pstmt.executeQuery();
			return rs;
		} catch (SQLException e) {
			throw new DBException(e, m_sql);
		}
		
	}
	
	public void close(){
		DB.close(rs, pstmt);
		rs = null;
		pstmt = null;
	}
}
