package org.compiere.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.CLogger;


public class Mticket_order extends X_fl_ticket_order{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6208882242042582467L;


    /** Load Constructor 
     * @throws Exception */
	public Mticket_order(Properties ctx, int fl_ticket_order_ID, String trxName) throws Exception {
		super(ctx, fl_ticket_order_ID, trxName);
	}
	
    /** Load Constructor 
     * @throws Exception */
    public Mticket_order (Properties ctx, ResultSet rs, String trxName) throws Exception
    {
      super (ctx, rs, trxName);
    }

	private static CLogger		s_log = CLogger.getCLogger (Mticket_order.class);

	public static List<X_fl_ticket_order> getTicketOrders(Properties ctx, int fl_ticket_id){
		final String whereClause = "fl_ticket_id=?  ";
		List<X_fl_ticket_order> retValue = new Query(ctx,I_fl_ticket_order.Table_Name,whereClause,null)
		.setParameters(fl_ticket_id)
		.list();
		return retValue;
	}
}
