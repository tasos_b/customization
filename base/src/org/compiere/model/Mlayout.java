package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.CLogger;
import org.compiere.util.Env;

public class Mlayout extends X_fl_layout{

    /** Load Constructor */
	public Mlayout(Properties ctx, int fl_layout_ID, String trxName) {
		super(ctx, fl_layout_ID, trxName);
	}
	
    /** Load Constructor */
    public Mlayout (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }


	/**
	 * 
	 */
	private static final long serialVersionUID = 3530096226239822438L;
	/**	Static Logger				*/
	private static CLogger		s_log = CLogger.getCLogger (Mlayout.class);

	
	public static Mlayout get (Properties ctx, int fl_terminal_id, String name){
		final String whereClause = "name=? AND AD_Client_ID=? AND fl_terminal_id=?";
		Mlayout retValue = new Query(ctx,I_fl_terminal.Table_Name,whereClause,null)
		.setParameters(name, Env.getAD_Client_ID(ctx), fl_terminal_id)
		.firstOnly();
		s_log.info("Mlayout get ");
		return retValue;

	}


}
