package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.fly.core.utils.ExceptionUtil;
import org.fly.core.utils.HashMapUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.core.utils.StringUtil;

public class Mticket extends X_fl_ticket {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4696041559471282107L;

	/**
	 * Load Constructor
	 * 
	 * @throws Exception
	 */
	public Mticket(Properties ctx, int fl_ticket_ID, String trxName)
			throws Exception {
		super(ctx, fl_ticket_ID, trxName);
	}

	/**
	 * Load Constructor
	 * 
	 * @throws Exception
	 */
	public Mticket(Properties ctx, ResultSet rs, String trxName)
			throws Exception {
		super(ctx, rs, trxName);
	}

	private static CLogger s_log = CLogger.getCLogger(Mticket.class);

	public static List<Mticket> getOpenTickets(Properties ctx,
			int fl_terminal_id) {
		final String whereClause = "fl_terminal_id=? AND AD_Client_ID=? AND isclosed='N' ";
		List<Mticket> retValue = new Query(ctx, I_fl_ticket.Table_Name,
				whereClause, null).setParameters(fl_terminal_id,
				Env.getAD_Client_ID(ctx)).list();
		return retValue;

	}

	public static Mticket[] get(Properties ctx, Timestamp dateOrderedFrom,
			Timestamp dateOrderedTo, String documentNo, String isClosed) {

		ArrayList<Object> prms = new ArrayList<Object>();
		StringBuffer whereClause = new StringBuffer(
				" IsActive='Y' AND IsClosed=" + isClosed + " ");
		if (!StringUtil.isNullOrEmpty(documentNo))
			whereClause.append("AND DocumentNo like'"
					+ getFindParameter(documentNo) + "' ");
		if (dateOrderedFrom != null) {
			whereClause.append("AND DateOrdered>=? ");
			prms.add(dateOrderedFrom);
		}
		if (dateOrderedTo != null) {
			whereClause.append("AND DateOrdered<=? ");
			prms.add(dateOrderedTo);
		}
		if (prms.size() > 0) {
			List<Mticket> q = new Query(ctx, I_fl_ticket.Table_Name,
					whereClause.toString(), null).setParameters(prms.toArray())
					.list();
			return q.toArray(new Mticket[q.size()]);
		} else {

			List<Mticket> q = new Query(ctx, I_fl_ticket.Table_Name,
					whereClause.toString(), null).list();
			return q.toArray(new Mticket[q.size()]);
		}
	}

	public static HashMap<String, Object> getM_PriceList_Version_ID(
			Properties ctx, int M_PriceList_ID, int C_BPartner_ID) {
		HashMap<String, Object> result = HashMapUtil.newHashMap();
		result.put("M_PriceList_Version_ID", 0);
		if (C_BPartner_ID != 0) {
			MBPartner m_bpartner = MBPartner.get(ctx, C_BPartner_ID);
			if (m_bpartner != null && m_bpartner.getM_PriceList_ID() != 0)
				M_PriceList_ID = m_bpartner.getM_PriceList_ID();
		}
		MPriceList pl = MPriceList.get(ctx, M_PriceList_ID, null);
		result.put("Currency",
				MCurrency.getISO_Code(ctx, pl.getC_Currency_ID()));
		result.put("PriceListName", pl.getName());
		MPriceListVersion plv = pl.getPriceListVersion(Env.getContextAsDate(
				ctx, "#Date"));
		if (plv != null && plv.getM_PriceList_Version_ID() != 0)
			result.put("M_PriceList_Version_ID",
					plv.getM_PriceList_Version_ID());
		return result;

	}

	public static BigDecimal getPrice(Properties ctx, int M_Product_ID,
			int M_PriceList_Version_ID, int M_Warehouse_ID) {
		MProduct product = MProduct.get(ctx, M_Product_ID);
		MWarehousePrice result = MWarehousePrice.get(product,
				M_PriceList_Version_ID, M_Warehouse_ID, null);
		if (result != null)
			return result.getPriceStd();
		else
			return Env.ZERO;
	}

	public static HashMap<String, Object> createTicket(Properties ctx,
			int fl_terminal_id, int AD_User_ID, int M_Warehouse_ID,
			int C_BPartner_ID, int M_PriceList_ID, int C_DocType_ID,
			int SalesRep_ID) {

		HashMap<String, Object> result = HashMapUtil.newHashMap();

		String uuid = UUID.randomUUID().toString();
		Trx trx = Trx.get(Trx.createTrxName("NewTicket" + uuid), true);
		trx.start();
		try {
			String missing = "";
			MOrder order = new MOrder(ctx, 0, trx.getTrxName());
			if (C_BPartner_ID == 0)
				missing += "Missing BPartner" + "\n";
			if (M_PriceList_ID == 0)
				missing += "Missing Price List" + "\n";
			if (M_Warehouse_ID == 0)
				missing += "Missing Warehouse" + "\n";
			if (AD_User_ID == 0)
				missing += "Missing User" + "\n";

			if (!missing.equals(""))
				throw new AdempiereException(missing);

			if (C_DocType_ID == 0)
				order.setC_DocTypeTarget_ID(MOrder.DocSubTypeSO_POS);
			else
				order.setC_DocTypeTarget_ID(C_DocType_ID);

			order.setC_BPartner_ID(C_BPartner_ID);
			order.setM_PriceList_ID(M_PriceList_ID);
			order.setM_Warehouse_ID(M_Warehouse_ID);
			order.setSalesRep_ID(SalesRep_ID);
			order.setPaymentRule(MOrder.PAYMENTRULE_Cash);
			order.setDeliveryRule(MOrder.DELIVERYRULE_Force);

			if (!order.save()) {
				throw new AdempiereException(
						ExceptionUtil
								.getLastExceptionMessage("Order not saved."));
			}
			result.put("C_Order_ID", order.get_ID());
			Mticket ticket = new Mticket(ctx, 0, trx.getTrxName());
			ticket.setfl_terminal_ID(fl_terminal_id);
			ticket.setC_Order_ID(order.get_ID());
			ticket.setDocumentNo(order.getDocumentNo());
			ticket.setDateOrdered(order.getDateOrdered());
			if (!ticket.save()) {
				throw new AdempiereException(
						ExceptionUtil
								.getLastExceptionMessage("Ticket not saved."));
			}
			Mticket_order ticket_order = new Mticket_order(ctx, 0,
					trx.getTrxName());
			ticket_order.setfl_ticket_ID(ticket.get_ID());
			ticket_order.setC_Order_ID(order.get_ID());
			if (!ticket_order.save()) {
				throw new AdempiereException(
						ExceptionUtil
								.getLastExceptionMessage("Ticket order not saved."));
			}
			trx.commit();
			result.put("fl_ticket_id", ticket.get_ID());
			return result;
		} catch (Exception e) {
			trx.rollback();
			result.put("Error", e.getMessage());
			s_log.severe(e.getMessage());
			return result;
		}

	}

	public static HashMap<String, Object> addTicketLine(Properties ctx,
			int C_Order_ID, int M_Product_ID, int C_BPartner_ID,
			int M_PriceList_Version_ID, int M_Warehouse_ID, BigDecimal qty) {
		String missing = "";
		if (C_Order_ID == 0)
			missing += "Ticket Not Found" + "\n";
		if (M_Product_ID == 0)
			missing += "Product Not Found" + "\n";
		if (C_BPartner_ID == 0)
			missing += "BPartner Not Found" + "\n";
		if (M_Warehouse_ID == 0)
			missing += "Warehouse Not Found" + "\n";

		HashMap<String, Object> result = HashMapUtil.newHashMap();

		if (!missing.equals("")) {
			result.put("Error", missing);
			return result;
		}
		MOrder order = new MOrder(ctx, C_Order_ID, null);
		if (!order.getDocStatus().equals("DR"))
			return result;
		MOrderLine line = new MOrderLine(order);
		line.setC_BPartner_ID(C_BPartner_ID);
		line.setM_Product_ID(M_Product_ID);
		BigDecimal price = getPrice(ctx, M_Product_ID, M_PriceList_Version_ID,
				M_Warehouse_ID);
		if (price != null && price.floatValue() != 0)
			line.setPrice(price);
		else
			line.setPrice();
		line.setQty(qty);
		line.setTax();
		if (!line.save()) {
			result.put("Error", ExceptionUtil
					.getLastExceptionMessage("Ticket order not saved."));

			return result;
		}
		return result;
	}

	public static HashMap<String, Object> changeTicketCustomer(Properties ctx,
			int C_Order_ID, int C_BPartner_ID) {
		HashMap<String, Object> result = HashMapUtil.newHashMap();
		String uuid = UUID.randomUUID().toString();
		Trx trx = Trx.get(Trx.createTrxName("changeTicketCustomer" + uuid),
				true);
		trx.start();
		try {
			MOrder order = new MOrder(ctx, C_Order_ID, null);

			MBPartner partner = new MBPartner(ctx, C_BPartner_ID, null);
			if (order.getDocStatus().equals("DR")) {
				if (partner == null || partner.get_ID() == 0) {
					throw new AdempiereException("no BPartner");
				} else {

					order.setBPartner(partner);
					MOrderLine[] lineas = order.getLines();
					for (int i = 0; i < lineas.length; i++) {
						lineas[i].setC_BPartner_ID(partner.getC_BPartner_ID());
						lineas[i].setTax();
						if (!lineas[i].save()) {
							result.put(
									"Error",
									ExceptionUtil
											.getLastExceptionMessage("Ticket order not saved."));

							break;
						}
					}
					if (!order.save()) {
						throw new AdempiereException(
								ExceptionUtil
										.getLastExceptionMessage("Order not saved."));
					}
				}
			}
			trx.commit();
		} catch (Exception e) {
			trx.rollback();
			result.put("Error", e.getMessage());
			s_log.severe(e.getMessage());
			return result;
		}

		return result;
	}

	public static HashMap<String, Object> deleteTicket(Properties ctx,
			int fl_ticket_id) {
		boolean force = true;
		HashMap<String, Object> result = HashMapUtil.newHashMap();
		String uuid = UUID.randomUUID().toString();
		Trx trx = Trx.get(Trx.createTrxName("changeTicketCustomer" + uuid),
				true);
		trx.start();
		try {
			ArrayList<Integer> ids = new ArrayList<Integer>();
			// delete ticket_orders
			List<X_fl_ticket_order> ticket_orders = Mticket_order
					.getTicketOrders(ctx, fl_ticket_id);
			for (X_fl_ticket_order tor : ticket_orders) {
				ids.add(tor.getC_Order_ID());
				tor.delete(force, trx.getTrxName());
				tor = null;

			}
			// delete ticket
			Mticket ticket = new Mticket(ctx, fl_ticket_id, null);
			ticket.delete(force, trx.getTrxName());
			// delete all orders
			for (int j = 0; j < ids.size(); j++) {

				int C_Order_ID = ids.get(j).intValue();

				MOrder order = new MOrder(ctx, C_Order_ID, null);
				MOrderTax[] taxs = order.getTaxes(true);
				MOrderLine[] lines = order.getLines();
				if (order.getDocStatus().equals("DR")) {
					if (taxs != null) {
						int numTax = taxs.length;
						if (numTax > 0)
							for (int i = taxs.length - 1; i >= 0; i--) {
								if (taxs[i] != null) {
									if (!taxs[i]
											.delete(force, trx.getTrxName()))
										throw new AdempiereException(
												ExceptionUtil
														.getLastExceptionMessage("Ticket not deleted."));
									taxs[i] = null;
								}
							}
					}
					if (lines != null) {
						int numLines = lines.length;
						if (numLines > 0)
							for (int i = numLines - 1; i >= 0; i--) {
								if (lines[i] != null) {
									if (!lines[i].delete(force,
											trx.getTrxName()))
										throw new AdempiereException(
												ExceptionUtil
														.getLastExceptionMessage("Ticket not deleted."));
									lines[i] = null;
								}
							}
					}
					order = new MOrder(ctx, C_Order_ID, null);
					if (!order.delete(force, trx.getTrxName())) {
						throw new AdempiereException(
								ExceptionUtil
										.getLastExceptionMessage("Order not deleted."));
					}
				}

			}

			trx.commit();
			return result;
		} catch (Exception e) {
			trx.rollback();
			result.put("Error", e.getMessage());
			s_log.severe(e.getMessage());
			return result;
		}

	}

	// ----------------------------------------------------------------------
	// ----- PAYMENTS
	// -----------------------------------------------------------------
	// ----------------------------------------------------------------------

	public static MPayment createPayment(Properties ctx,
			HashMap<String, Object> result, int AD_Org_ID, int C_Order_ID,
			int C_BPartner_ID, String tenderType) {
		MPayment payment = new MPayment(ctx, 0, null);
		try {

			payment.setAD_Org_ID(AD_Org_ID);
			payment.setTenderType(tenderType);
			payment.setC_Order_ID(C_Order_ID);
			payment.setIsReceipt(true);
			payment.setC_BPartner_ID(C_BPartner_ID);
		} catch (Exception ex) {
			result.put("Error", ex.getLocalizedMessage());
			return null;
		}
		return payment;
	}


	public static int getC_Invoice_ID(int C_Order_ID) {
		String sql = "SELECT C_Invoice_ID FROM C_Invoice "
				+ "WHERE C_Order_ID=? AND DocStatus IN ('CO','CL') "
				+ "ORDER BY C_Invoice_ID DESC";
		int C_Invoice_ID = DB.getSQLValue(null, sql, C_Order_ID);
		return C_Invoice_ID;
	} // getC_Invoice_ID

	public static BigDecimal getPaidAmt(int C_Order_ID) {
		String sql = "SELECT sum(PayAmt) FROM C_Payment WHERE C_Order_ID = ? AND DocStatus IN ('CO','CL')";
		BigDecimal received = DB.getSQLValueBD(null, sql, C_Order_ID);
		if (received == null)
			received = Env.ZERO;

		sql = "SELECT sum(Amount) FROM C_CashLine WHERE C_Invoice_ID = ? ";
		BigDecimal cashline = DB.getSQLValueBD(null, sql,
				getC_Invoice_ID(C_Order_ID));
		if (cashline != null)
			received = received.add(cashline);

		return received;
	}

	public static BigDecimal getGrandTotal(int C_Order_ID) {

		String sql = "SELECT grandtotal " + "from fl_ticket_order_infos_v "
				+ "WHERE c_order_id=?";
		SQLUtil util = new SQLUtil(sql, null, C_Order_ID);

		try {
			ResultSet rs = util.getResultSet();
			while (rs.next()) {
				return rs.getBigDecimal("grandtotal");

			}
			return null;
		} catch (SQLException e) {
			throw new DBException(e, sql);
		} finally {
			util.close();
		}

	}

	public static BigDecimal getBalanceDue(int C_Order_ID) {
		return getGrandTotal(C_Order_ID).subtract(getPaidAmt(C_Order_ID));
	}

	
	public static HashMap<String, Object> cashPayoff(Properties ctx,
			int AD_Org_ID, int C_Order_ID, int C_BPartner_ID,
			int C_CashBook_ID, int C_Currency_ID, int C_BankAccount_ID) {

		HashMap<String, Object> result = HashMapUtil.newHashMap();

		BigDecimal balanceDue = getBalanceDue(C_Order_ID);
		if (balanceDue.doubleValue() == 0) {
			result.put("Info", "Ticket has zero balance");
			return result;
		}
		try {
			MPayment payment = createPayment(ctx, result, AD_Org_ID,
					C_Order_ID, C_BPartner_ID, MPayment.TENDERTYPE_Cash);

			payment.setC_CashBook_ID(C_CashBook_ID);
			payment.setAmount(C_Currency_ID, balanceDue);
			payment.setC_BankAccount_ID(C_BankAccount_ID);
			if (!payment.save()) {
				result.put("Error", ExceptionUtil
						.getLastExceptionMessage("Ticket payment not saved."));
			}
			payment.setDocAction(MPayment.DOCACTION_Complete);
			payment.setDocStatus(MPayment.DOCSTATUS_Drafted);
			if (payment.processIt(MPayment.DOCACTION_Complete)) {
				if (!payment.save()) {
					result.put(
							"Error",
							ExceptionUtil
									.getLastExceptionMessage("Ticket payment not saved."));
				}
			}
		} catch(Exception ex) {
			return result;
		}
		return result;
	}
	public static HashMap<String, Object> cashPartialPayment(Properties ctx,
			int AD_Org_ID, int C_Order_ID, int C_BPartner_ID,
			int C_CashBook_ID, int C_Currency_ID, int C_BankAccount_ID, BigDecimal amt) {

		HashMap<String, Object> result = HashMapUtil.newHashMap();

		BigDecimal balanceDue = getBalanceDue(C_Order_ID);
		if (balanceDue.doubleValue() == 0) {
			result.put("Info", "Ticket has zero balance");
			return result;
		}
		if (amt.doubleValue() == 0) {
			result.put("Error", "Zero pament not allowed");
			return result;
		}
		if (amt.doubleValue() > balanceDue.doubleValue()) {
			result.put("Error", "Payment is bigger than balance due");
			return result;
		}
		try {
			MPayment payment = createPayment(ctx, result, AD_Org_ID,
					C_Order_ID, C_BPartner_ID, MPayment.TENDERTYPE_Cash);

			payment.setC_CashBook_ID(C_CashBook_ID);
			payment.setAmount(C_Currency_ID, amt);
			payment.setC_BankAccount_ID(C_BankAccount_ID);
			if (!payment.save()) {
				result.put("Error", ExceptionUtil
						.getLastExceptionMessage("Ticket payment not saved."));
			}
			payment.setDocAction(MPayment.DOCACTION_Complete);
			payment.setDocStatus(MPayment.DOCSTATUS_Drafted);
			if (payment.processIt(MPayment.DOCACTION_Complete)) {
				if (!payment.save()) {
					result.put(
							"Error",
							ExceptionUtil
									.getLastExceptionMessage("Ticket payment not saved."));
				}
			}
		} catch(Exception ex) {
			return result;
		}
		return result;
	}
	
	public static HashMap<String, Object> cardPartialPayment(Properties ctx,
			int AD_Org_ID, int C_Order_ID, int C_BPartner_ID,
			int C_Currency_ID, int C_BankAccount_ID, BigDecimal amt, String cardType) {

		HashMap<String, Object> result = HashMapUtil.newHashMap();

		BigDecimal balanceDue = getBalanceDue(C_Order_ID);
		if (balanceDue.doubleValue() == 0) {
			result.put("Info", "Ticket has zero balance");
			return result;
		}
		if (amt.doubleValue() == 0) {
			result.put("Error", "Zero pament not allowed");
			return result;
		}
		if (amt.doubleValue() > balanceDue.doubleValue()) {
			result.put("Error", "Payment is bigger than balance due");
			return result;
		}
		try {
			MPayment payment = createPayment(ctx, result, AD_Org_ID,
					C_Order_ID, C_BPartner_ID, MPayment.TENDERTYPE_CreditCard);

			payment.setAmount(C_Currency_ID, amt);
			payment.setC_BankAccount_ID(C_BankAccount_ID);
			payment.setTenderType(MPayment.TENDERTYPE_CreditCard);
			payment.setTrxType(MPayment.TRXTYPE_Sales);
			payment.setCreditCardType (cardType);
			if (!payment.save()) {
				result.put("Error", ExceptionUtil
						.getLastExceptionMessage("Ticket payment not saved."));
			}
			payment.setDocAction(MPayment.DOCACTION_Complete);
			payment.setDocStatus(MPayment.DOCSTATUS_Drafted);
			if (payment.processIt(MPayment.DOCACTION_Complete)) {
				if (!payment.save()) {
					result.put(
							"Error",
							ExceptionUtil
									.getLastExceptionMessage("Ticket payment not saved."));
				}
			}
		} catch(Exception ex) {
			return result;
		}
		return result;
	}
	
	

}
