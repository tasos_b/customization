package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.Query;
import org.compiere.util.Env;

public class Mterminal extends X_fl_terminal{

    /** Load Constructor 
     * @throws Exception */
	public Mterminal(Properties ctx, int fl_terminal_ID, String trxName) throws Exception {
		super(ctx, fl_terminal_ID, trxName);
	}
	
    /** Load Constructor 
     * @throws Exception */
    public Mterminal (Properties ctx, ResultSet rs, String trxName) throws Exception
    {
      super (ctx, rs, trxName);
    }


	/**
	 * 
	 */
	private static final long serialVersionUID = 3530096226249822438L;

	/**	Static Logger				*/
	//private static CLogger		s_log = CLogger.getCLogger (Mterminal.class);

	
	public static Mterminal get (Properties ctx, String name){
		final String whereClause = "name=? AND AD_Client_ID=?";
		Mterminal retValue = new Query(ctx,I_fl_terminal.Table_Name,whereClause,null)
		.setParameters(name, Env.getAD_Client_ID(ctx))
		.firstOnly();
		return retValue;

	}


}
