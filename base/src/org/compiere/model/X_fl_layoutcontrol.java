/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2016 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for fl_layoutcontrol
 *  @author Adempiere (generated) 
 *  @version Release 3.8.0 - $Id$ */
public class X_fl_layoutcontrol extends PO implements I_fl_layoutcontrol, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160714L;

    /** Standard Constructor */
    public X_fl_layoutcontrol (Properties ctx, int fl_layoutcontrol_ID, String trxName)
    {
      super (ctx, fl_layoutcontrol_ID, trxName);
      /** if (fl_layoutcontrol_ID == 0)
        {
			setcontrol_height (0);
// 200
			setcontrol_width (0);
// 200
			setcontrol_x (0);
			setcontrol_y (0);
			setfl_control_ID (0);
			setfl_layoutcontrol_ID (0);
			setfl_layout_ID (0);
			setName (null);
        } */
    }

    /** Load Constructor */
    public X_fl_layoutcontrol (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_fl_layoutcontrol[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Process getAD_Process() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Process)MTable.get(getCtx(), org.compiere.model.I_AD_Process.Table_Name)
			.getPO(getAD_Process_ID(), get_TrxName());	}

	/** Set Process.
		@param AD_Process_ID 
		Process or Report
	  */
	public void setAD_Process_ID (int AD_Process_ID)
	{
		if (AD_Process_ID < 1) 
			set_Value (COLUMNNAME_AD_Process_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Process_ID, Integer.valueOf(AD_Process_ID));
	}

	/** Get Process.
		@return Process or Report
	  */
	public int getAD_Process_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Process_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set control_height.
		@param control_height control_height	  */
	public void setcontrol_height (int control_height)
	{
		set_Value (COLUMNNAME_control_height, Integer.valueOf(control_height));
	}

	/** Get control_height.
		@return control_height	  */
	public int getcontrol_height () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_control_height);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set control_width.
		@param control_width control_width	  */
	public void setcontrol_width (int control_width)
	{
		set_Value (COLUMNNAME_control_width, Integer.valueOf(control_width));
	}

	/** Get control_width.
		@return control_width	  */
	public int getcontrol_width () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_control_width);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set control_x.
		@param control_x control_x	  */
	public void setcontrol_x (int control_x)
	{
		set_Value (COLUMNNAME_control_x, Integer.valueOf(control_x));
	}

	/** Get control_x.
		@return control_x	  */
	public int getcontrol_x () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_control_x);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set control_y.
		@param control_y control_y	  */
	public void setcontrol_y (int control_y)
	{
		set_Value (COLUMNNAME_control_y, Integer.valueOf(control_y));
	}

	/** Get control_y.
		@return control_y	  */
	public int getcontrol_y () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_control_y);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set data.
		@param data data	  */
	public void setdata (byte[] data)
	{
		set_Value (COLUMNNAME_data, data);
	}

	/** Get data.
		@return data	  */
	public byte[] getdata () 
	{
		return (byte[])get_Value(COLUMNNAME_data);
	}

	public I_fl_control getfl_control() throws RuntimeException
    {
		return (I_fl_control)MTable.get(getCtx(), I_fl_control.Table_Name)
			.getPO(getfl_control_ID(), get_TrxName());	}

	/** Set Controls for FLYPos ID.
		@param fl_control_ID Controls for FLYPos ID	  */
	public void setfl_control_ID (int fl_control_ID)
	{
		if (fl_control_ID < 1) 
			set_Value (COLUMNNAME_fl_control_ID, null);
		else 
			set_Value (COLUMNNAME_fl_control_ID, Integer.valueOf(fl_control_ID));
	}

	/** Get Controls for FLYPos ID.
		@return Controls for FLYPos ID	  */
	public int getfl_control_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_control_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Layout Control ID.
		@param fl_layoutcontrol_ID Layout Control ID	  */
	public void setfl_layoutcontrol_ID (int fl_layoutcontrol_ID)
	{
		if (fl_layoutcontrol_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_fl_layoutcontrol_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_fl_layoutcontrol_ID, Integer.valueOf(fl_layoutcontrol_ID));
	}

	/** Get Layout Control ID.
		@return Layout Control ID	  */
	public int getfl_layoutcontrol_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_layoutcontrol_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_fl_layout getfl_layout() throws RuntimeException
    {
		return (I_fl_layout)MTable.get(getCtx(), I_fl_layout.Table_Name)
			.getPO(getfl_layout_ID(), get_TrxName());	}

	/** Set Fly Layout ID.
		@param fl_layout_ID Fly Layout ID	  */
	public void setfl_layout_ID (int fl_layout_ID)
	{
		if (fl_layout_ID < 1) 
			set_Value (COLUMNNAME_fl_layout_ID, null);
		else 
			set_Value (COLUMNNAME_fl_layout_ID, Integer.valueOf(fl_layout_ID));
	}

	/** Get Fly Layout ID.
		@return Fly Layout ID	  */
	public int getfl_layout_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_layout_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set runbutton.
		@param runbutton runbutton	  */
	public void setrunbutton (String runbutton)
	{
		set_Value (COLUMNNAME_runbutton, runbutton);
	}

	/** Get runbutton.
		@return runbutton	  */
	public String getrunbutton () 
	{
		return (String)get_Value(COLUMNNAME_runbutton);
	}
}