/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2016 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for fl_control
 *  @author Adempiere (generated) 
 *  @version Release 3.8.0 - $Id$ */
public class X_fl_control extends PO implements I_fl_control, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160714L;

    /** Standard Constructor */
    public X_fl_control (Properties ctx, int fl_control_ID, String trxName)
    {
      super (ctx, fl_control_ID, trxName);
      /** if (fl_control_ID == 0)
        {
			setfl_control_ID (0);
			setName (null);
        } */
    }

    /** Load Constructor */
    public X_fl_control (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_fl_control[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Controls for FLYPos ID.
		@param fl_control_ID Controls for FLYPos ID	  */
	public void setfl_control_ID (int fl_control_ID)
	{
		if (fl_control_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_fl_control_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_fl_control_ID, Integer.valueOf(fl_control_ID));
	}

	/** Get Controls for FLYPos ID.
		@return Controls for FLYPos ID	  */
	public int getfl_control_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_control_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }
}