/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2016 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

/** Generated Model for fl_ticket
 *  @author Adempiere (generated) 
 *  @version Release 3.8.0 - $Id$ */
public class X_fl_ticket extends PO implements I_fl_ticket, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160714L;

    /** Standard Constructor */
    public X_fl_ticket (Properties ctx, int fl_ticket_ID, String trxName)
    {
      super (ctx, fl_ticket_ID, trxName);
      /** if (fl_ticket_ID == 0)
        {
			setfl_ticket_ID (0);
			setIsClosed (false);
        } */
    }

    /** Load Constructor */
    public X_fl_ticket (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_fl_ticket[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_Value (COLUMNNAME_C_Order_ID, null);
		else 
			set_Value (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_OrderLine getC_OrderLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_OrderLine)MTable.get(getCtx(), org.compiere.model.I_C_OrderLine.Table_Name)
			.getPO(getC_OrderLine_ID(), get_TrxName());	}

	/** Set Sales Order Line.
		@param C_OrderLine_ID 
		Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID)
	{
		if (C_OrderLine_ID < 1) 
			set_Value (COLUMNNAME_C_OrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_OrderLine_ID, Integer.valueOf(C_OrderLine_ID));
	}

	/** Get Sales Order Line.
		@return Sales Order Line
	  */
	public int getC_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Ordered.
		@param DateOrdered 
		Date of Order
	  */
	public void setDateOrdered (Timestamp DateOrdered)
	{
		set_Value (COLUMNNAME_DateOrdered, DateOrdered);
	}

	/** Get Date Ordered.
		@return Date of Order
	  */
	public Timestamp getDateOrdered () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateOrdered);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_Value (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	public I_fl_terminal getfl_terminal() throws RuntimeException
    {
		return (I_fl_terminal)MTable.get(getCtx(), I_fl_terminal.Table_Name)
			.getPO(getfl_terminal_ID(), get_TrxName());	}

	/** Set Terminals for FLYpos ID.
		@param fl_terminal_ID Terminals for FLYpos ID	  */
	public void setfl_terminal_ID (int fl_terminal_ID)
	{
		if (fl_terminal_ID < 1) 
			set_Value (COLUMNNAME_fl_terminal_ID, null);
		else 
			set_Value (COLUMNNAME_fl_terminal_ID, Integer.valueOf(fl_terminal_ID));
	}

	/** Get Terminals for FLYpos ID.
		@return Terminals for FLYpos ID	  */
	public int getfl_terminal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_terminal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set fl_ticket ID.
		@param fl_ticket_ID fl_ticket ID	  */
	public void setfl_ticket_ID (int fl_ticket_ID)
	{
		if (fl_ticket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_fl_ticket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_fl_ticket_ID, Integer.valueOf(fl_ticket_ID));
	}

	/** Get fl_ticket ID.
		@return fl_ticket ID	  */
	public int getfl_ticket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_ticket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Closed Status.
		@param IsClosed 
		The status is closed
	  */
	public void setIsClosed (boolean IsClosed)
	{
		set_Value (COLUMNNAME_IsClosed, Boolean.valueOf(IsClosed));
	}

	/** Get Closed Status.
		@return The status is closed
	  */
	public boolean isClosed () 
	{
		Object oo = get_Value(COLUMNNAME_IsClosed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}
}