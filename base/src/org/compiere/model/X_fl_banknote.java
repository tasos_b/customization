/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2016 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for fl_banknote
 *  @author Adempiere (generated) 
 *  @version Release 3.8.0 - $Id$ */
public class X_fl_banknote extends PO implements I_fl_banknote, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160714L;

    /** Standard Constructor */
    public X_fl_banknote (Properties ctx, int fl_banknote_ID, String trxName)
    {
      super (ctx, fl_banknote_ID, trxName);
      /** if (fl_banknote_ID == 0)
        {
			setfl_banknote_ID (0);
			setfl_payment_setup_ID (0);
			setLine (0);
			setmoneyvalue (Env.ZERO);
			setName (null);
        } */
    }

    /** Load Constructor */
    public X_fl_banknote (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_fl_banknote[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Image.
		@param AD_Image_ID 
		Image or Icon
	  */
	public void setAD_Image_ID (int AD_Image_ID)
	{
		if (AD_Image_ID < 1) 
			set_Value (COLUMNNAME_AD_Image_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Image_ID, Integer.valueOf(AD_Image_ID));
	}

	/** Get Image.
		@return Image or Icon
	  */
	public int getAD_Image_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Image_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set fl_banknote ID.
		@param fl_banknote_ID fl_banknote ID	  */
	public void setfl_banknote_ID (int fl_banknote_ID)
	{
		if (fl_banknote_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_fl_banknote_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_fl_banknote_ID, Integer.valueOf(fl_banknote_ID));
	}

	/** Get fl_banknote ID.
		@return fl_banknote ID	  */
	public int getfl_banknote_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_banknote_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_fl_payment_setup getfl_payment_setup() throws RuntimeException
    {
		return (I_fl_payment_setup)MTable.get(getCtx(), I_fl_payment_setup.Table_Name)
			.getPO(getfl_payment_setup_ID(), get_TrxName());	}

	/** Set fl_payment_setup ID.
		@param fl_payment_setup_ID fl_payment_setup ID	  */
	public void setfl_payment_setup_ID (int fl_payment_setup_ID)
	{
		if (fl_payment_setup_ID < 1) 
			set_Value (COLUMNNAME_fl_payment_setup_ID, null);
		else 
			set_Value (COLUMNNAME_fl_payment_setup_ID, Integer.valueOf(fl_payment_setup_ID));
	}

	/** Get fl_payment_setup ID.
		@return fl_payment_setup ID	  */
	public int getfl_payment_setup_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_payment_setup_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_Value (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set moneyvalue.
		@param moneyvalue moneyvalue	  */
	public void setmoneyvalue (BigDecimal moneyvalue)
	{
		set_Value (COLUMNNAME_moneyvalue, moneyvalue);
	}

	/** Get moneyvalue.
		@return moneyvalue	  */
	public BigDecimal getmoneyvalue () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_moneyvalue);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }
}