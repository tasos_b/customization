package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.DBException;
import org.compiere.process.ProcessInfo;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.FLYProcessUtil;

public class Mprocess extends X_fl_process{

    /** Load Constructor */
	public Mprocess(Properties ctx, int fl_process_ID, String trxName) {
		super(ctx, fl_process_ID, trxName);
	}
	
    /** Load Constructor */
    public Mprocess (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }


	private static final long serialVersionUID = 3530086226249822438L;
	/**	Static Logger				*/
	private static CLogger		s_log = CLogger.getCLogger (Mprocess.class);

	public static Mprocess get (Properties ctx, String name){
		final String whereClause = "name=? AND AD_Client_ID=?";
		Mprocess retValue = new Query(ctx,I_fl_process.Table_Name,whereClause,null)
		.setParameters(name, Env.getAD_Client_ID(ctx))
		.firstOnly();
		return retValue;

	}

	public boolean executeProcess(ProcessInfo piExtra, Trx trx){
		/*
		 * .Extract processinfo, if no data then create a new
		 * .Merge processinfo with the extra params
		 * .Get the process class name from AD_Process
		 * .Execute
		 * */
		ProcessInfo pi = null;
		if(getdata() != null)
			pi = ByteArrayUtil.byteArrayToProcessInfo(getdata());
		else
			pi = new ProcessInfo(getName(), getAD_Client_ID());
		if(piExtra != null)
			pi = FLYProcessUtil.mergeProcessInfos(pi, piExtra);
		String classname = getFlProcessClass(getAD_Process_ID());
		if(classname == null || classname.isEmpty())
			return false;
		return internalExecuteProcess(classname, pi, trx);
	}

	
	
	private boolean internalExecuteProcess(String classname, ProcessInfo pi, Trx trx){
		SvrProcess process = null;
		try
		{
			Class<?> clazz = Class.forName(classname);
			process = (SvrProcess)clazz.newInstance();
			process.startProcess(Env.getCtx(), pi,  trx);
			return true;
		}
		catch (Exception e)
		{
			s_log.log(Level.SEVERE, e.getMessage(), e);
			return false;
		}
		
	}

	private String getFlProcessClass(int AD_Process_ID){
		String sql = 	"SELECT classname " 	+ 
						"FROM AD_Process "		+
						"WHERE AD_Process_ID=?"; //1
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, AD_Process_ID);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				return rs.getString("Classname");
			}
		}
		catch(SQLException e)
		{
			throw new DBException(e, sql);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return null;

	}
	
}
