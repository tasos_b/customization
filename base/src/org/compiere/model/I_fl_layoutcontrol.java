/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2016 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for fl_layoutcontrol
 *  @author Adempiere (generated) 
 *  @version Release 3.8.0
 */
public interface I_fl_layoutcontrol 
{

    /** TableName=fl_layoutcontrol */
    public static final String Table_Name = "fl_layoutcontrol";

    /** AD_Table_ID=1000011 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_Process_ID */
    public static final String COLUMNNAME_AD_Process_ID = "AD_Process_ID";

	/** Set Process.
	  * Process or Report
	  */
	public void setAD_Process_ID (int AD_Process_ID);

	/** Get Process.
	  * Process or Report
	  */
	public int getAD_Process_ID();

	public org.compiere.model.I_AD_Process getAD_Process() throws RuntimeException;

    /** Column name control_height */
    public static final String COLUMNNAME_control_height = "control_height";

	/** Set control_height	  */
	public void setcontrol_height (int control_height);

	/** Get control_height	  */
	public int getcontrol_height();

    /** Column name control_width */
    public static final String COLUMNNAME_control_width = "control_width";

	/** Set control_width	  */
	public void setcontrol_width (int control_width);

	/** Get control_width	  */
	public int getcontrol_width();

    /** Column name control_x */
    public static final String COLUMNNAME_control_x = "control_x";

	/** Set control_x	  */
	public void setcontrol_x (int control_x);

	/** Get control_x	  */
	public int getcontrol_x();

    /** Column name control_y */
    public static final String COLUMNNAME_control_y = "control_y";

	/** Set control_y	  */
	public void setcontrol_y (int control_y);

	/** Get control_y	  */
	public int getcontrol_y();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name data */
    public static final String COLUMNNAME_data = "data";

	/** Set data	  */
	public void setdata (byte[] data);

	/** Get data	  */
	public byte[] getdata();

    /** Column name fl_control_ID */
    public static final String COLUMNNAME_fl_control_ID = "fl_control_ID";

	/** Set Controls for FLYPos ID	  */
	public void setfl_control_ID (int fl_control_ID);

	/** Get Controls for FLYPos ID	  */
	public int getfl_control_ID();

	public I_fl_control getfl_control() throws RuntimeException;

    /** Column name fl_layoutcontrol_ID */
    public static final String COLUMNNAME_fl_layoutcontrol_ID = "fl_layoutcontrol_ID";

	/** Set Layout Control ID	  */
	public void setfl_layoutcontrol_ID (int fl_layoutcontrol_ID);

	/** Get Layout Control ID	  */
	public int getfl_layoutcontrol_ID();

    /** Column name fl_layout_ID */
    public static final String COLUMNNAME_fl_layout_ID = "fl_layout_ID";

	/** Set Fly Layout ID	  */
	public void setfl_layout_ID (int fl_layout_ID);

	/** Get Fly Layout ID	  */
	public int getfl_layout_ID();

	public I_fl_layout getfl_layout() throws RuntimeException;

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name runbutton */
    public static final String COLUMNNAME_runbutton = "runbutton";

	/** Set runbutton	  */
	public void setrunbutton (String runbutton);

	/** Get runbutton	  */
	public String getrunbutton();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
