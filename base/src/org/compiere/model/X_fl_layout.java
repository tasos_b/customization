/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2016 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for fl_layout
 *  @author Adempiere (generated) 
 *  @version Release 3.8.0 - $Id$ */
public class X_fl_layout extends PO implements I_fl_layout, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160714L;

    /** Standard Constructor */
    public X_fl_layout (Properties ctx, int fl_layout_ID, String trxName)
    {
      super (ctx, fl_layout_ID, trxName);
      /** if (fl_layout_ID == 0)
        {
			setfl_layout_ID (0);
			setfl_terminal_ID (0);
			setName (null);
        } */
    }

    /** Load Constructor */
    public X_fl_layout (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_fl_layout[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Fly Layout ID.
		@param fl_layout_ID Fly Layout ID	  */
	public void setfl_layout_ID (int fl_layout_ID)
	{
		if (fl_layout_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_fl_layout_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_fl_layout_ID, Integer.valueOf(fl_layout_ID));
	}

	/** Get Fly Layout ID.
		@return Fly Layout ID	  */
	public int getfl_layout_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_layout_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_fl_terminal getfl_terminal() throws RuntimeException
    {
		return (I_fl_terminal)MTable.get(getCtx(), I_fl_terminal.Table_Name)
			.getPO(getfl_terminal_ID(), get_TrxName());	}

	/** Set Terminals for FLYpos ID.
		@param fl_terminal_ID Terminals for FLYpos ID	  */
	public void setfl_terminal_ID (int fl_terminal_ID)
	{
		if (fl_terminal_ID < 1) 
			set_Value (COLUMNNAME_fl_terminal_ID, null);
		else 
			set_Value (COLUMNNAME_fl_terminal_ID, Integer.valueOf(fl_terminal_ID));
	}

	/** Get Terminals for FLYpos ID.
		@return Terminals for FLYpos ID	  */
	public int getfl_terminal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_terminal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }
}