/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2016 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for fl_layoutcontrol_button
 *  @author Adempiere (generated) 
 *  @version Release 3.8.0 - $Id$ */
public class X_fl_layoutcontrol_button extends PO implements I_fl_layoutcontrol_button, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160714L;

    /** Standard Constructor */
    public X_fl_layoutcontrol_button (Properties ctx, int fl_layoutcontrol_button_ID, String trxName)
    {
      super (ctx, fl_layoutcontrol_button_ID, trxName);
      /** if (fl_layoutcontrol_button_ID == 0)
        {
			setfl_layoutcontrol_button_ID (0);
			setfl_layoutcontrol_ID (0);
			setName (null);
        } */
    }

    /** Load Constructor */
    public X_fl_layoutcontrol_button (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_fl_layoutcontrol_button[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set buttoninfo.
		@param buttoninfo buttoninfo	  */
	public void setbuttoninfo (String buttoninfo)
	{
		set_Value (COLUMNNAME_buttoninfo, buttoninfo);
	}

	/** Get buttoninfo.
		@return buttoninfo	  */
	public String getbuttoninfo () 
	{
		return (String)get_Value(COLUMNNAME_buttoninfo);
	}

	/** Set fl_layoutcontrol_button ID.
		@param fl_layoutcontrol_button_ID fl_layoutcontrol_button ID	  */
	public void setfl_layoutcontrol_button_ID (int fl_layoutcontrol_button_ID)
	{
		if (fl_layoutcontrol_button_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_fl_layoutcontrol_button_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_fl_layoutcontrol_button_ID, Integer.valueOf(fl_layoutcontrol_button_ID));
	}

	/** Get fl_layoutcontrol_button ID.
		@return fl_layoutcontrol_button ID	  */
	public int getfl_layoutcontrol_button_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_layoutcontrol_button_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_fl_layoutcontrol getfl_layoutcontrol() throws RuntimeException
    {
		return (I_fl_layoutcontrol)MTable.get(getCtx(), I_fl_layoutcontrol.Table_Name)
			.getPO(getfl_layoutcontrol_ID(), get_TrxName());	}

	/** Set Layout Control ID.
		@param fl_layoutcontrol_ID Layout Control ID	  */
	public void setfl_layoutcontrol_ID (int fl_layoutcontrol_ID)
	{
		if (fl_layoutcontrol_ID < 1) 
			set_Value (COLUMNNAME_fl_layoutcontrol_ID, null);
		else 
			set_Value (COLUMNNAME_fl_layoutcontrol_ID, Integer.valueOf(fl_layoutcontrol_ID));
	}

	/** Get Layout Control ID.
		@return Layout Control ID	  */
	public int getfl_layoutcontrol_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_layoutcontrol_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_fl_process_group getfl_process_group() throws RuntimeException
    {
		return (I_fl_process_group)MTable.get(getCtx(), I_fl_process_group.Table_Name)
			.getPO(getfl_process_group_ID(), get_TrxName());	}

	/** Set fl_process_group ID.
		@param fl_process_group_ID fl_process_group ID	  */
	public void setfl_process_group_ID (int fl_process_group_ID)
	{
		if (fl_process_group_ID < 1) 
			set_Value (COLUMNNAME_fl_process_group_ID, null);
		else 
			set_Value (COLUMNNAME_fl_process_group_ID, Integer.valueOf(fl_process_group_ID));
	}

	/** Get fl_process_group ID.
		@return fl_process_group ID	  */
	public int getfl_process_group_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_process_group_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_fl_process getfl_process() throws RuntimeException
    {
		return (I_fl_process)MTable.get(getCtx(), I_fl_process.Table_Name)
			.getPO(getfl_process_ID(), get_TrxName());	}

	/** Set fl_process ID.
		@param fl_process_ID fl_process ID	  */
	public void setfl_process_ID (int fl_process_ID)
	{
		if (fl_process_ID < 1) 
			set_Value (COLUMNNAME_fl_process_ID, null);
		else 
			set_Value (COLUMNNAME_fl_process_ID, Integer.valueOf(fl_process_ID));
	}

	/** Get fl_process ID.
		@return fl_process ID	  */
	public int getfl_process_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fl_process_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_Value (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }
}