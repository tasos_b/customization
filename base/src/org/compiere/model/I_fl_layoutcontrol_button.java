/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2016 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for fl_layoutcontrol_button
 *  @author Adempiere (generated) 
 *  @version Release 3.8.0
 */
public interface I_fl_layoutcontrol_button 
{

    /** TableName=fl_layoutcontrol_button */
    public static final String Table_Name = "fl_layoutcontrol_button";

    /** AD_Table_ID=1000023 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name buttoninfo */
    public static final String COLUMNNAME_buttoninfo = "buttoninfo";

	/** Set buttoninfo	  */
	public void setbuttoninfo (String buttoninfo);

	/** Get buttoninfo	  */
	public String getbuttoninfo();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name fl_layoutcontrol_button_ID */
    public static final String COLUMNNAME_fl_layoutcontrol_button_ID = "fl_layoutcontrol_button_ID";

	/** Set fl_layoutcontrol_button ID	  */
	public void setfl_layoutcontrol_button_ID (int fl_layoutcontrol_button_ID);

	/** Get fl_layoutcontrol_button ID	  */
	public int getfl_layoutcontrol_button_ID();

    /** Column name fl_layoutcontrol_ID */
    public static final String COLUMNNAME_fl_layoutcontrol_ID = "fl_layoutcontrol_ID";

	/** Set Layout Control ID	  */
	public void setfl_layoutcontrol_ID (int fl_layoutcontrol_ID);

	/** Get Layout Control ID	  */
	public int getfl_layoutcontrol_ID();

	public I_fl_layoutcontrol getfl_layoutcontrol() throws RuntimeException;

    /** Column name fl_process_group_ID */
    public static final String COLUMNNAME_fl_process_group_ID = "fl_process_group_ID";

	/** Set fl_process_group ID	  */
	public void setfl_process_group_ID (int fl_process_group_ID);

	/** Get fl_process_group ID	  */
	public int getfl_process_group_ID();

	public I_fl_process_group getfl_process_group() throws RuntimeException;

    /** Column name fl_process_ID */
    public static final String COLUMNNAME_fl_process_ID = "fl_process_ID";

	/** Set fl_process ID	  */
	public void setfl_process_ID (int fl_process_ID);

	/** Get fl_process ID	  */
	public int getfl_process_ID();

	public I_fl_process getfl_process() throws RuntimeException;

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
