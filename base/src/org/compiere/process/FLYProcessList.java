/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/


package org.compiere.process;

import java.util.ArrayList;
import java.util.UUID;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.fly.core.utils.FLYProcessUtil;

/**
 * FLYProcessList is a sequence of action to be executed
 * Parameters are initialized with Env Parameters
 * If action has stored parameters then these override the env parameters
 * Any result of an execution is passed to the next  
 * 
 * @author tasos boulassikis tasosbull@gmail.com    
 *
 */
public class FLYProcessList {

	ArrayList<FLYProcess> m_processList = new ArrayList<FLYProcess>();
	boolean m_trx;

	public FLYProcessList(boolean transactional) {
		m_trx = transactional;
	}

	public void add(FLYProcess process) {
		m_processList.add(process);
	}

	public ProcessInfo execute(ProcessInfo envParams) {
		Trx trx = null;
		if (m_trx) {
			String uuid = UUID.randomUUID().toString();
			trx = Trx.get(Trx.createTrxName("NewTicket" + uuid), true);
			trx.start();
		}
		try {
			ProcessInfo pi;
			if(envParams == null)
				pi = new ProcessInfo("ProcessInfo", 0);
			else
				pi = envParams;
			for (int i = 0; i < m_processList.size(); i++) {
				FLYProcess process = m_processList.get(i);
				//override the env params with the process params
				pi = FLYProcessUtil.mergeProcessInfos(pi, process.getM_pi() );
				pi.setAD_Process_ID(process.getM_pi().getAD_Process_ID());
				try {
					Class<?> clazz = Class.forName(process.getM_class());
					Object object = clazz.newInstance();
					ProcessCall srvp = (ProcessCall) object;
					srvp.startProcess(Env.getCtx(), pi, trx);
				} catch (Exception ex) {
					throw new AdempiereException(ex);
				}
			}
			
			if(trx != null){
				trx.commit();
			}
			return pi;
			
		} catch (Exception e) {
			if(trx != null){
				trx.rollback();
				throw new AdempiereException(e);
			}

		}
		return null;
	}
}