/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.compiere.process;

import java.util.HashMap;
import org.compiere.model.Mticket;
import org.compiere.util.Env;
import org.fly.core.utils.ExceptionUtil;

public class NewTicketProcess extends FLYSvrProcess {
	private int C_BPartner_ID;
	private int fl_terminal_id;
	private int AD_User_ID;
	private int M_PriceList_ID;
	private int M_Warehouse_ID;
	private int C_DocType_ID;
	private int SalesRep_ID;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_BPartner_ID"))
				C_BPartner_ID = para[i].getParameterAsInt();
			else if (name.equals("AD_User_ID"))
				AD_User_ID = para[i].getParameterAsInt();
			else if (name.equals("fl_terminal_id"))
				fl_terminal_id = para[i].getParameterAsInt();
			else if (name.equals("M_PriceList_ID"))
				M_PriceList_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Warehouse_ID"))
				M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("C_DocType_ID"))
				C_DocType_ID = para[i].getParameterAsInt();
			else if (name.equals("SalesRep_ID"))
				SalesRep_ID = para[i].getParameterAsInt();
		}

	}

	@Override
	protected String doIt() throws Exception {
		HashMap<String, Object> result = Mticket.createTicket(Env.getCtx(),
				fl_terminal_id, AD_User_ID, M_Warehouse_ID, C_BPartner_ID,
				M_PriceList_ID, C_DocType_ID, SalesRep_ID);
		String error = (String) result.get("Error");
		if (ExceptionUtil.isError(error)) {
			setError(error);
		} else {
			setParameter("C_Order_ID", result.get("C_Order_ID"));
			setParameter("fl_ticket_id", result.get("fl_ticket_id"));
		}
		return null;
	}

}
