package org.compiere.process;

import java.math.BigDecimal;
import java.util.HashMap;

import org.compiere.model.Mticket;
import org.compiere.util.Env;
import org.fly.core.utils.ExceptionUtil;

public class CardPartialPaymentProcess extends FLYSvrProcess{

	int AD_Org_ID = 0;
	int C_Order_ID = 0;
	int C_BPartner_ID = 0;
	int C_Currency_ID = 0;
	int C_BankAccount_ID = 0;
	BigDecimal Amount = null;
	String CardType = null;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("AD_Org_ID"))
				AD_Org_ID = para[i].getParameterAsInt();
			else if (name.equals("C_Order_ID"))
				C_Order_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				C_BPartner_ID = para[i].getParameterAsInt();
			else if (name.equals("CardType"))
				CardType = para[i].getParameterAsString();
			else if (name.equals("C_Currency_ID"))
				C_Currency_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BankAccount_ID"))
				C_BankAccount_ID = para[i].getParameterAsInt();
			else if (name.equals("Amount"))
				Amount = para[i].getParameterAsBigDecimal();
		}

		
		
	}

	@Override
	protected String doIt() throws Exception {
		HashMap<String, Object> result = Mticket.cardPartialPayment(Env.getCtx(),
				AD_Org_ID, C_Order_ID, C_BPartner_ID,
				 C_Currency_ID, C_BankAccount_ID, Amount, CardType);
		String error = (String) result.get("Error");
		if (ExceptionUtil.isError(error)) {
			setError(error);
		}
		return null;
	}

}
