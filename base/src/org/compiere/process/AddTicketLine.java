/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.compiere.process;

import java.util.HashMap;

import org.compiere.model.Mticket;
//import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.fly.core.utils.ExceptionUtil;

public class AddTicketLine extends FLYSvrProcess {

	// private static CLogger s_log = CLogger.getCLogger(AddTicketLine.class);

	int C_Order_ID = 0;
	int M_Product_ID = 0;
	int C_BPartner_ID = 0;
	int M_PriceList_Version_ID = 0;
	int M_Warehouse_ID = 0;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_Order_ID"))
				C_Order_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_ID"))
				M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				C_BPartner_ID = para[i].getParameterAsInt();
			else if (name.equals("M_PriceList_Version_ID"))
				M_PriceList_Version_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Warehouse_ID"))
				M_Warehouse_ID = para[i].getParameterAsInt();
		}

	}

	@Override
	protected String doIt() throws Exception {
		HashMap<String, Object> result = Mticket.addTicketLine(Env.getCtx(),
				C_Order_ID, M_Product_ID, C_BPartner_ID,
				M_PriceList_Version_ID, M_Warehouse_ID, Env.ONE);
		String error = (String) result.get("Error");
		if (ExceptionUtil.isError(error)) {
			setError(error);
		}
		return null;
	}

}
