/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.compiere.process;

public class FLYProcess {
	
	ProcessInfo m_pi = null;
	String m_class = null;
	int AD_Process_ID;
	
	public FLYProcess(ProcessInfo pi, String clazz, int AD_Process_ID){
		m_pi = pi;
		m_class = clazz;
		this.AD_Process_ID = AD_Process_ID;
	}
	
	public ProcessInfo getM_pi() {
		return m_pi;
	}
	
	public String getM_class() {
		return m_class;
	}
	
}
